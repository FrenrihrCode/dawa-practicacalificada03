const express = require('express');
const socketIO = require('socket.io');
const path = require('path');
const http = require('http');
const {Usuarios} = require('./classes/usuarios');
const {crearMensaje} = require('./utilidades/utilidades');

const app = express();
let server = http.createServer(app);

const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;
//express
app.use(express.static(publicPath));
app.get('/', (req, res) => {
    res.sendFile(publicPath + '/index.html')
});
app.get('/chat', (req, res) => {
    res.sendFile(publicPath + '/chat.html')
});
//IO comunicación con el cliente
const io = socketIO(server);
const usuarios = new Usuarios();
//Conexión con socket
io.on('connection', (socket)=>{
    //Esta función recibirá un mensaje y lo enviará a todos los usuarios de chat
    socket.on('welcome', (data) => {
        socket.broadcast.emit('message', data)
    });
    socket.on('message', (data) => {
        socket.broadcast.emit('message', data)
    });
    socket.on('disconnect', () => {
        console.log("Se ha desconectado un usuario")
    });
    socket.on('leave', (data) => {
        socket.broadcast.emit('leave', data)
    });
});
/*
io.on('connection', (client)=>{
    console.log('Usuario conectado');
    client.on('entrarChat', (data, callback)=>{
        if(!data.nombre){
            return callback({
                error: true,
                mensaje: "El nombre es necesario"
            });
        }
        let personas = usuarios.agregarPersona(client.id, data.nombre);
        client.broadcast.emit("listaPersonas", usuarios.getPersonas());
        callback(personas);
    });

    client.on('crearMensaje', (data) => {
        let persona = usuarios.getPersona(client.id);
        let mensaje = crearMensaje(persona.nombre, data.mensaje);
        client.broadcast.emit("crearMensaje", mensaje);
    })

    client.on('disconnect', () =>{
        let personaBorrada = usuarios.borrarPersona(client.id);
        client.broadcast.emit('crearMensaje', {
            usuario: 'Administrador',
            mensaje: crearMensaje('Admin', `${personaBorrada.nombre} ha salido.`)
        });
        client.broadcast.emit("listaPersonas", usuarios.getPersonas());
    })
});
*/
server.listen(port, (err) => {
    if (err) throw new Error(err);
    console.log(`Servidor corriendo en puerto ${ port }`);
});