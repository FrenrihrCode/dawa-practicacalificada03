var socket = io();
//utilizamos las funciones JS BOM para redirigir o obtener el nombre
var params = new URLSearchParams(window.location.search);
//redirigir en caso de que no exista algun nombre
if(!params.has("nombre") || params.get('nombre') == ''){
    window.location.href = "/";
    throw new Error("El nombre es necesario");
} else {
    
    //obetener el nombre del usuario
    var usuario = {
        nombre: params.get("nombre")
    };
    //funciones del socket, en escucha
    socket.on("connect", function(){
        console.log("Conectado al servidor");
        socket.emit("entrarChat", usuario, function(resp){
            console.log("Usuarios conectados: ", resp);
        });
    });

    socket.on('disconnect', function() {
        console.log('Perdimos conexion con el servidor')
    });

    socket.on('crearMensaje', function(mensaje) {
        console.log('Servidor mensaje: ', mensaje)
    });

    socket.on('listaPersonas', function(personas) {
        console.log(personas)
    });

}